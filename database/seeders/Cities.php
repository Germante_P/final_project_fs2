<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Cities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cities() as $city) {
            DB::table('cities')->insert([
                'title' => $city
            ]);
        }
    }
    private function cities(): array
    {
        return [
            'Bath',
            'Birmingham',
            'Bradford',
            'Brighton and Hove',
            'Bristol',
            'Cambridge',
            'Canterbury',
            'Carlisle',
            'Chester',
            'Chelmsford',
            'Chichester',
            'Coventry',
            'Derby',
            'Durham',
            'Ely',
            'Exeter',
            'Gloucester',
            'Hereford',
            'Kingston upon Hull',
            'Lancaster',
            'Leeds',
            'Leicester',
            'Lichfield',
            'Lincoln',
            'Liverpool',
            'City of London',
            'Manchester',
            'Newcastle upon Tyne',
            'Norwich',
            'Nottingham',
            'Oxford',
            'Peterborough',
            'Plymouth',
            'Portsmouth',
            'Preston',
            'Ripon',
            'Salford',
            'Salisbury',
            'Sheffield',
            'Southampton',
            'St Albans',
            'Stoke-on-Trent',
            'Sunderland',
            'Truro',
            'Wakefield',
            'Wells',
            'Westminster',
            'Winchester',
            'Wolverhampton',
            'Worcester',
            'York'
        ];
    }
}
