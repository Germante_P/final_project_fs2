<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('design_image', function (Blueprint $table) {

            $table->unsignedBigInteger('design_id')->unsigned();
            $table->unsignedBigInteger('image_id')->unsigned();

            $table->foreign('design_id')
                ->references('id')
                ->on('designs');
            $table->foreign('image_id')
                ->references('id')
                ->on('images');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('design_image');
    }
}
