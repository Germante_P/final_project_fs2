<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DesignImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'image_id',
        'design_id'
    ];

    protected $table = 'design_image';


    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }

    /**
     * @return HasMany
     */
    public function designs(): HasMany
    {
        return $this->hasMany(Design::class);
    }

}
