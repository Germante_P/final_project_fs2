<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'path'
    ];

    /**
     * @param Request $request
     * @return Collection
     */
    public static function upload(Request $request): Collection
    {
        $request->validate([
            'imageFile' => 'required',
        ]);

        $imageIds = collect([]);
        if ($request->hasfile('imageFile')) {
            foreach ($request->file('imageFile') as $file) {
                $name = $file->getClientOriginalName();
                $file->move(public_path() . '/storage/uploads/images/', $name);
                $imageIds[] = Image::create(['title' => $name, 'path' => $name])->id;
            }
            return $imageIds;
        }
    }
}
