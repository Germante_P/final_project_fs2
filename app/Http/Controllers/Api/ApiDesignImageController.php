<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\DesignImage;
use Illuminate\Http\JsonResponse;

class ApiDesignImageController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function getImage(): JsonResponse
    {
        $data = DesignImage::get();
        return response()->json($data);
    }

}
