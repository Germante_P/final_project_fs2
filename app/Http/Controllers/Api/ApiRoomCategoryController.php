<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\RoomCategory;
use Illuminate\Http\JsonResponse;

class ApiRoomCategoryController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = RoomCategory::get();
        return response()->json($data);
    }

}
