<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateConsultationRequestsRequest;
use App\Models\ConsultationRequest;
use Illuminate\Http\RedirectResponse;

class ApiConsultationRequestController extends Controller
{
    /**
     * @param UpdateConsultationRequestsRequest $request
     * @return RedirectResponse
     */
    public function store(UpdateConsultationRequestsRequest $request): RedirectResponse
    {
       return ConsultationRequest::create($request->all())->toJson();
    }
}
