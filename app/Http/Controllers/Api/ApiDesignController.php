<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DesignResource;
use App\Models\Design;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ApiDesignController extends Controller
{
    /**
     * @param Request $request
     * @return Application|ResponseFactory|AnonymousResourceCollection|Response
     */
    public function index(Request $request): Response
    {
        $categoryId = $request->input('category');
        $designs = Design::with('images');

        if ($categoryId) {
            $designs = $designs->where('room_category_id', '=', $categoryId);
        }

        return response($designs->get());
    }

    /**
     * @param $id
     * @return DesignResource
     */
    public function show($id): DesignResource
    {
        return new DesignResource(Design::findOrFail($id));
    }

}
