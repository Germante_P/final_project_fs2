<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\JsonResponse;

class ApiCityController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function getCities(): JsonResponse
    {
        $data = City::get();
        return response()->json($data);
    }

}
