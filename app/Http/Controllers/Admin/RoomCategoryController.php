<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateRoomCategoriesRequest;
use App\Models\RoomCategory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class RoomCategoryController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $room_categories = RoomCategory::get();
        return view('admin.room_categories.index', compact('room_categories'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return $this->edit(new RoomCategory());
    }

    /**
     * @param RoomCategory $roomCategory
     * @return View
     */
    public function edit(RoomCategory $roomCategory): View
    {
        $model = $roomCategory;
        return view('admin.room_categories.edit', compact('model'));
    }

    /**
     * @param UpdateRoomCategoriesRequest $request
     * @return RedirectResponse
     */
    public function store(UpdateRoomCategoriesRequest $request): RedirectResponse
    {
        return $this->update($request, new RoomCategory());
    }

    /**
     * @param UpdateRoomCategoriesRequest $request
     * @param RoomCategory $roomCategory
     * @return RedirectResponse
     */
    public function update(UpdateRoomCategoriesRequest $request, RoomCategory $roomCategory): RedirectResponse
    {
        $roomCategory->fill($request->all())->save();
        return redirect()->route('admin.room_categories.index');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $room_category = RoomCategory::find($id);
        $room_category->delete();

        return redirect()->route('admin.room_categories.index');
    }

}
