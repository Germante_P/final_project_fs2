<?php

namespace App\Http\Controllers\Admin\Designs;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateConsultationRequestsRequest;
use App\Models\City;
use App\Models\ConsultationRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ConsultationRequestController extends Controller
{
    /**
     * @return view|Application|Factory
     */
    public function index()
    {
       $requests =  ConsultationRequest::with('roomCategory')->get();
        return view('admin.requests.index', compact('requests'));
    }

    /**
     * @param UpdateConsultationRequestsRequest $request
     * @return RedirectResponse
     */
    public function store(UpdateConsultationRequestsRequest $request): RedirectResponse
    {
        City::all();
        ConsultationRequest::create($request->all());
        return redirect()->to('/#/consultation');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $consultation_request = ConsultationRequest::find($id);
        $consultation_request->delete();

        return redirect()->route('admin.requests.index');
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id){
        $model = ConsultationRequest::where('id', $id)->first();
        return view('admin.requests.show', compact('model'));
    }
}
