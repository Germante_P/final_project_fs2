<?php

namespace App\Http\Controllers\Admin\Designs;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateDesignsRequest;
use App\Models\Design;
use App\Models\Image;
use App\Models\RoomCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;


class DesignController extends Controller
{
    /**
     * @return View
     */
    public function index(): view
    {
        $room_categories = RoomCategory::get();
        $images = Image::get();
        $designs = Design::with('roomCategory', 'images')->get();
        return view('admin.designs.index', compact('designs', 'room_categories', 'images'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return $this->edit(new Design());
    }

    /**
     * @param Design $design
     * @return View
     */
    public function edit(Design $design): View
    {
        $model = $design;
        $room_categories = RoomCategory::get();
        $image = Image::all();
        return view('admin.designs.edit', compact('model', 'room_categories', 'image'));
    }

    /**
     * @param UpdateDesignsRequest $request
     * @return RedirectResponse
     */
    public function store(UpdateDesignsRequest $request): RedirectResponse
    {
        return $this->update($request, new Design());
    }

    /**
     * @param UpdateDesignsRequest $request
     * @param Design $design
     * @return Application|Factory|View|RedirectResponse
     */
    public function update(UpdateDesignsRequest $request, Design $design)
    {
        $design->fill($request->all())->save();
        $imageIds = Image::upload($request);
        $design->images()->sync($imageIds);
        return redirect()->route('admin.designs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function show(int $id)
    {
        $room_categories = RoomCategory::get();
        $design_category = Design::where('room_category_id', $id)->get();

        return view('admin.designs.show', compact('design_category', 'room_categories'));
    }

    /**
     * @param Design $design
     * @return RedirectResponse
     */
    public function destroy(Design $design): RedirectResponse
    {
        $design->images()->delete();
        $design->delete();
        return redirect()->back();
    }
}
