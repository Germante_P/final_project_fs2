<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;


class HomeController extends Controller
{
    /**
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function index()
    {
        $user = User::get();
        return view('admin.home', compact('user'));
    }
}
