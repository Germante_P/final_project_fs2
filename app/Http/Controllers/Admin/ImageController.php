<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ImageController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(): view
    {
        return view('admin.images');
    }

}
