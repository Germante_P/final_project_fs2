<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUsersRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        $users = User::get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        return $this->edit(new User());
    }

    /**
     * @param User $user
     * @return View
     */
    public function edit(User $user): View
    {
        $model = $user;
        return view('admin.users.edit', compact('model'));
    }

    /**
     * @param UpdateUsersRequest $request
     * @return View
     */
    public function store(UpdateUsersRequest $request): View
    {
        return $this->update($request, new User());
    }

    /**
     * @param UpdateUsersRequest $request
     * @param User $user
     * @return View
     */
    public function update(UpdateUsersRequest $request, User $user): View
    {
        $user->fill($request->all())->save();
        return $this->index();
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('admin.users.index');
    }

}
