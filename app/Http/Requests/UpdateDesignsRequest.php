<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDesignsRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'year' => 'required|digits:4',
            'author' => 'required|string|max:255',
            'room_category_id' => 'required|exists:room_categories,id',
            'imageFile' => 'required',
        ];
    }
}
