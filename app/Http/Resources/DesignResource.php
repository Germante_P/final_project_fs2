<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DesignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
          'id' => $this->id,
          'room_category_id' => $this->room_category_id,
            'title'=> $this->title,
            'description'=> $this->description,
            'year'=> $this->year,
            'author'=> $this->author,
            'images'=> $this->images,
        ];
    }
}
