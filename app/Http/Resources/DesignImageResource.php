<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class DesignImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'design_id' => $this->design_id,
            'image_id' => $this->image_id,
        ];
    }
}
