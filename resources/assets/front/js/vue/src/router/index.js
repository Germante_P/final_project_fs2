import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../views/Main.vue';

Vue.use(VueRouter, {
    mode: 'history'
});

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main,
  },
  {
    path: '/projects',
    name: 'Projects',
    component: () => import('../views/Projects.vue'),
  },
    {
        path: '/projects/:id',
        name: 'Single Project',
        component: () => import('../views/SingleProject.vue'),
    },
    {
        path: '/room_categories/:id',
        name: 'Single Room Category',
        component: () => import('../views/SingleRoomCategory.vue'),
    },
    {
    path: '/consultation',
    name: 'Consultation',
    component: () => import('../views/Consultation.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
