@extends('admin.layouts.page',['content_title'=>trans('app.users')])

@section('content')
    <?php
    if($model->exists){
        $action = 'Edit User';
        $route = route('admin.users.update',$model);
    }else{
        $action = 'Create User';
        $route = route('admin.users.store');
    }
    ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{$action}}</h3>
                        </div>

                        <form action="{{$route}}" method="POST">
                            @if($model->exists)
                                @method('PUT')
                            @endif
                            @csrf()
                            <div class="card-body row">
                                <div class="form-group col-4">
                                    <label for="first_name">First name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value= "{{$model->first_name ?? ''}}" placeholder="First name">
                                </div>

                                <div class="form-group col-4">
                                    <label for="last_name">Last name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" value= "{{$model->last_name ?? ''}}" placeholder="Last name">
                                </div>

                                <div class="form-group col-4">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" value= "{{$model->email ?? ''}}" placeholder="Email">
                                </div>

                                <div class="form-group col-4">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" name="phone" id="phone" value= "{{$model->phone ?? ''}}" placeholder="Phone">
                                </div>

                                <div class="form-group col-4">
                                    <label for="address">Address</label>
                                    <input type="text" class="form-control" name="address" id="address" value= "{{$model->address ?? ''}}" placeholder="Address">
                                </div>

                                <div class="form-group col-4">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" value= "{{$model->password ?? ''}}" placeholder="Password">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
