@extends('admin.layouts.page',['content_title'=>trans('app.users')])

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div>
                                <a href="{{route('admin.users.create')}}" class="btn btn-primary float-left">Add new user</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="user_table_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="user_table"
                                               class="table table-bordered table-hover dataTable dtr-inline"
                                               role="grid" aria-describedby="example2_info">
                                            <thead class="thead-light">
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" rowspan="1" colspan="1"
                                                    aria-sort="ascending">
                                                    No.
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    First name
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Last name
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Email
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Phone
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Address
                                                </th>
                                                <th class="sorting sorting_asc" rowspan="1" colspan="1"
                                                    aria-sort="ascending">
                                                    Created at
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody cellpadding="0">
                                            @foreach($users as $user)
                                                <tr class="odd">
                                                    <td>{{$user->first_name ?? ''}}</td>
                                                    <td>{{$user->last_name ?? ''}}</td>
                                                    <td>{{$user->email ?? ''}}</td>
                                                    <td>{{$user->phone ?? ''}}</td>
                                                    <td>{{$user->address ?? ''}}</td>
                                                    <td>{{$user->created_at ?? ''}}</td>
                                                    <td>
                                                        <div class="row mx-auto">
                                                        <a class="btn btn-actions px-3"
                                                           href="{{route('admin.users.edit', $user->id)}}">Edit</a>
                                                        <form action="{{route('admin.users.destroy', $user->id)}}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="submit" class="btn btn-actions mx-3" value='Delete'>
                                                        </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
