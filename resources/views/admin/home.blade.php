@extends('admin.layouts.page')

@section('content')
        <section class="content hero">
            <div class="container-fluid">
                <h1 class="text-uppercase py-5 text-center">Welcome, {{{ isset(Auth::user()->name) ? Auth::user()->first_name : Auth::user()->first_name }}} !
                </h1>
            </div>
        </section>
@endsection
