@extends('admin.layouts.page',['content_title'=>trans('app.room_categories')])

@section('content')
    <?php
    if($model->exists){
        $action = 'Edit Room';
        $route = route('admin.room_categories.update',$model);
    }else{
        $action = 'Create room';
        $route = route('admin.room_categories.store');
    }
    ?>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{$action}}</h3>
                        </div>

                        <form action="{{$route}}" method="POST">
                            @if($model->exists)
                                @method('PUT')
                            @endif
                            @csrf()
                            <div class="card-body row">
                                <div class="form-group col-4">
                                    <label for="title">Room name</label>
                                    <input type="text" class="form-control" name="title" id="title" value= "{{$model->title ?? ''}}" placeholder="Title">
                                    <div class="alert-danger">{{ $errors->first('title') }}</div>
                                </div>

                                <div class="form-group col-8">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" name="description" id="description" rows="5" placeholder="Description">{{$model->description ?? ''}}</textarea>
                                    <div class="alert-danger">{{ $errors->first('description') }}</div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
