@extends('admin.layouts.page',['content_title'=>trans('app.room_categories')])

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div>
                                <a href="{{route('admin.room_categories.create')}}" class="btn btn-primary float-left">Create new room category</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="room_categories_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="room_categories_table"
                                               class="table table-bordered table-hover dataTable dtr-inline"
                                               role="grid" aria-describedby="example2_info">
                                            <thead class="thead-light">
                                            <tr role="row">
                                                <th class="sorting sorting_asc col-1" tabindex="0" rowspan="1" colspan="1"
                                                    aria-sort="ascending">
                                                    No.
                                                </th>
                                                <th class="sorting col-3" rowspan="1" colspan="1">
                                                    Room name
                                                </th>
                                                <th class="sorting col-6" rowspan="1" colspan="">
                                                    Description
                                                </th>
                                                <th class="sorting col-2" rowspan="1" colspan="1">
                                                    Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody cellpadding="0">
                                            @foreach($room_categories as $room_category)
                                                <tr class="odd">
                                                    <td>{{$room_category->title ?? ''}}</td>
                                                    <td>{{$room_category->description ?? ''}}</td>
                                                    <td>
                                                        <div class="row mx-auto">
                                                            <a class="btn btn-actions px-3"
                                                               href="{{route('admin.room_categories.edit', $room_category->id)}}">Edit</a>
                                                            <form action="{{route('admin.room_categories.destroy', $room_category->id)}}" method="POST">
                                                                @csrf
                                                                @method('DELETE')
                                                                <input type="submit" class="btn btn-actions mx-3" value='Delete'>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

