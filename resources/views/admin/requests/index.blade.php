@extends('admin.layouts.page')

@section('content')

    <section>
        <div class="col-11 mx-auto">
            <div class="">
                <div class="">
                    <div class="card mx-auto">
                        <div class="card-body bg-primary text-white mailbox-widget pb-0">
                            <h2 class="text-white pb-3">Requests</h2>
                        </div>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade active show" id="inbox" aria-labelledby="inbox-tab" role="tabpanel">
                                <div>
                                    <div class="row p-4 no-gutters">

                                    </div>
                                    <div class="table-responsive">
                                        <table class="table email-table no-wrap table-hover v-middle mb-0 font-14">
                                            <thead class="thead-light">
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" rowspan="1" colspan="1"
                                                    aria-sort="ascending">
                                                    No.
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Date
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Name
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Email
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Subject
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Category
                                                </th>
                                                <th class="sorting" rowspan="1" colspan="1">
                                                    Actions
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody cellpadding="0">
                                            @foreach($requests as $request)
                                                <tr class='clickable-row' data-href='{{route('admin.requests.show', $request->id)}}'>
                                                    <td>
                                                        <span class="mb-0 text-muted">{{ $request->created_at }}</span>
                                                    </td>
                                                    <td>
                                                        <span class="mb-0 text-muted">{{$request->first_name}} {{$request->last_name}}</span>
                                                    </td>
                                                    <td>
                                                        <span class="mb-0 text-muted">{{$request->email}}</span>
                                                    </td>
                                                <td>
                                                        <span class="text-dark">{{$request->subject}}</span>
                                                </td>
                                                    <td>
                                                        <span class="mb-0 text-muted">{{$request->roomCategory->title ?? ''}}</span>
                                                    </td>
                                                <td>
                                                    <form action="{{route('admin.requests.destroy', $request->id)}}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="submit" class="btn btn-actions mx-3" value='Delete'>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
