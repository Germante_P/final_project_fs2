@extends('admin.layouts.page')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form method="GET" class="my-5">


                            <card>
                                <div class="mx-5">
                                    <div>
                                        <h2>{{$model->subject}}</h2>
                                        <h6 class="float-right">{{$model->created_at}}</h6>
                                    </div>
                                    <div>
                                        <h5> {{$model->first_name}} {{$model->last_name}} </h5>
                                        <h6>< {{$model->email}} > </h6>
                                        <h6> {{$model->phone}} </h6>
                                    </div>
                                    <div>
                                        <h6>Category: <span
                                                class="font-weight-bold"> {{$model->roomCategory->title}} </span></h6>
                                    </div>
                                </div>
                            </card>

                            <b-card-text>
                                <div class="container my-5">
                                    <h5> {{$model->message}}</h5>
                                </div>
                            </b-card-text>
                            <div>
                                <a href="{{route('admin.requests.index')}}" class="btn btn-actions mx-3">
                                    Back to requests
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
