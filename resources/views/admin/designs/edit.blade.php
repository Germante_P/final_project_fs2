@extends('admin.layouts.page',['content_title'=>''])

@section('content')
    <?php
    if ($model->exists) {
        $action = 'Edit Design';
        $route = route('admin.designs.update', $model);
        $required = '*Showing old images. Required to choose new one.';
    } else {
        $action = 'Create Design';
        $route = route('admin.designs.store');
        $required = '';
    }
    ?>
    <section>
        <div class="container">
            <div class=" text-center mt-5 ">
                <h1>{{$action}}</h1>
            </div>
            <div class="row ">
                <div class="col-lg-7 mx-auto">
                    <div class="card mt-2 mx-auto p-4 bg-light">
                        <div class="card-body bg-light">
                            <div class="container">
                                <form action="{{$route}}" method="POST" enctype="multipart/form-data">
                                    @if($model->exists)
                                        @method('PUT')
                                    @endif
                                    @csrf()
                                    <div class="controls">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="title">Title</label>
                                                    <input id="title" type="text" name="title" class="form-control"
                                                           placeholder="Enter title" value="{{$model->title ?? ''}}">

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="date">Project year</label>
                                                    <input type="text" name="year"
                                                           class="form-control date-picker" placeholder="Select year"
                                                           autocomplete="off" value="{{$model->year ?? ''}}">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group"><label for="author">Author</label>
                                                    <input id="author" type="text" name="author" class="form-control"
                                                           placeholder="Enter author" value="{{$model->author ?? ''}}">

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group"><label for="form_need">Select room</label>
                                                    <select id="room_categories" name="room_category_id"
                                                            class="form-control">
                                                        <option value="" selected disabled>--Select Room--</option>
                                                        @foreach($room_categories as $room_category)
                                                            <option {{$room_category->id === $model->room_category_id ?
                                                            'selected' : ''}} value="{{$room_category->id}}">
                                                                {{$room_category->title}}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea id="description" name="description" class="form-control"
                                                              rows="4">
                                                    {{$model->description ?? ''}}
                                                </textarea>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <p>{{$required}}</p>
                                                <div class="container d-flex row my-2">
                                                    @foreach($model->images as $image)
                                                        <div class="image_block mr-2">
                                                            <a href="/storage/uploads/images/{{$image->title ?? ''}}">
                                                                <img
                                                                    src="/storage/uploads/images/{{$image->title ?? ''}}"
                                                                    alt="not available" width="100"/>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="custom-file">
                                                    <input type="file" name="imageFile[]" class="custom-file-input"
                                                           id="images" multiple="multiple">

                                                    <label class="custom-file-label" for="images">Choose image</label>
                                                </div>
                                                <div class="user-image mb-3 text-center">
                                                    <div class="imgPreview"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                    <div class="row mt-3">
                                        <button type="submit" class="btn btn-primary mx-auto">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

