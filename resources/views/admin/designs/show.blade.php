@extends('admin.layouts.page',['content_title'=>trans('app.designs')])

@section('content')
    <div class="card-body">
        <div id="designs" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12">
                    <table id="designs"
                           class="table table-bordered table-hover dataTable dtr-inline"
                           role="grid" aria-describedby="example2_info" style="table-layout:fixed;">
                        <thead class="thead-light">
                        <tr role="row">
                            <th class="sorting sorting_asc col-1" tabindex="0" rowspan="1" colspan="1"
                                aria-sort="ascending">
                                No.
                            </th>
                            <th class="sorting col-1" rowspan="1" colspan="1">
                                Room category
                            </th>
                            <th class="sorting col-2" rowspan="1" colspan="">
                                Title
                            </th>
                            <th class="sorting col-4" rowspan="1" colspan="">
                                Description
                            </th>

                            <th class="sorting col-1" rowspan="1" colspan="">
                                Year
                            </th>
                            <th class="sorting col-1" rowspan="1" colspan="">
                                Author
                            </th>
                            <th class="sorting col-2" rowspan="1" colspan="">
                                Images
                            </th>

                            <th class="sorting col-1" rowspan="1" colspan="1">
                                Actions
                            </th>
                        </tr>
                        </thead>
                        <tbody cellpadding="0">
                        @foreach($design_category as $design)
                            <tr class="odd">
                                <td>{{$design->roomCategory->title ?? ''}}</td>
                                <td>{{$design->title ?? ''}}</td>
                                <td style="word-wrap: break-word;">{{$design->description ?? ''}}</td>
                                <td>{{$design->year ?? ''}}</td>
                                <td>{{$design->author ?? ''}}</td>
                                <td>
                                    @foreach($design->images as $image)
                                        <a href="/storage/uploads/images/{{$image->title ?? ''}}" class="table_image_link">
                                            <img src="/storage/uploads/images/{{$image->title ?? ''}}" alt="not available" width="80"/>
                                        </a>
                                    @endforeach
                                </td>
                                <td>
                                    <div class=" mx-auto">
                                        <a class="btn btn-actions mb-2"
                                           href="{{route('admin.designs.edit', $design->id)}}">Edit</a>
                                        <form action="{{route('admin.designs.destroy', $design->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-actions" value='Delete'>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
