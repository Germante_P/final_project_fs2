@extends('admin.layouts.page',['content_title'=>trans('app.designs')])

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div>
                                <a href="{{route('admin.designs.create')}}" class="btn btn-primary float-left">Create
                                    new design</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="room_categories_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="container">
                                        <ul class="list-group">
                                            <li class="list-group-item active">Choose room category</li>
                                            @foreach($room_categories as $room_category)
                                                <li class="list-group-item">
                                                    <a href="{{url('admin/designs/'.$room_category->id)}}">
                                                        {{$room_category->title ?? ''}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
