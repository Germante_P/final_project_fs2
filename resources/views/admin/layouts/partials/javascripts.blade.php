<script src="{{asset('assets/admin/js/app.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins.js')}}"></script>

<script>
    jQuery(document).ready(function ($) {
        $(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $('.date-picker').datepicker({
            changeYear: true,
            changeMonth: false,
            showButtonPanel: true,
            dateFormat: 'yy',
            yearRange: 'c-10:c+0',
            onClose: function (dateText, inst) {
                $(this).datepicker('setDate', new Date(inst.selectedYear, 1));
            }
        });
    });
</script>

<!-- upload images  -->
<script>
    $(function () {
        var multiImgPreview = function (input, imgPreviewPlaceholder) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };
        $('#images').on('change', function () {
            multiImgPreview(this, 'div.imgPreview');
        });
    });
</script>



