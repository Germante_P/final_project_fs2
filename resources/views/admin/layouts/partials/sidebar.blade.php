<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="/storage/uploads/images/16x16_black2.png">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('admin.users.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.room_categories.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-couch"></i>
                        <p>
                            Room categories
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.designs.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-ruler-combined"></i>
                        <p>
                            Design Projects
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('admin.requests.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-inbox"></i>
                        <p>
                            Consultation requests
                        </p>
                    </a>
                </li>

                <!-- logout -->
                <li class="nav-item">
                    <form id="logout" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                    <a href="#logout" onclick="event.preventDefault();document.getElementById('logout').submit()" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            @lang('app.logout')
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
