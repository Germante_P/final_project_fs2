<?php

use App\Http\Controllers\Admin\Designs\ConsultationRequestController;
use App\Http\Controllers\Admin\Designs\DesignController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\RoomCategoryController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});

Route::prefix('admin')
    ->middleware('auth')
    ->name('admin.')->group(function(){
    Route::get('/', [HomeController::class, 'index']);
    Route::resource('users', UserController::class);
    Route::resource('room_categories', RoomCategoryController::class);
    Route::resource('designs',DesignController::class);
    Route::resource('requests',ConsultationRequestController::class);
    Route::resource('images',ImageController::class);
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


