<?php

use App\Http\Controllers\Api\ApiCityController;
use App\Http\Controllers\Api\ApiConsultationRequestController;
use App\Http\Controllers\Api\ApiDesignController;
use App\Http\Controllers\Api\ApiDesignImageController;
use App\Http\Controllers\Api\ApiImageController;
use App\Http\Controllers\Api\ApiRoomCategoryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/')->name('api.')->group(function () {
    Route::prefix('designs')->name('designs.')->group(function () {
        Route::get('/', [ApiDesignController::class, 'index']);
        Route::get('/{id}', [ApiDesignController::class, 'show']);
    });
});

Route::prefix('/')->name('api.')->group(function () {
    Route::prefix('room_categories')->name('room_categories.')->group(function () {
        Route::get('/', [ApiRoomCategoryController::class, 'index']);
    });

    Route::prefix('consultation_requests')->name('consultation_requests.')->group(function () {
        Route::post('/', [ApiConsultationRequestController::class, 'store']);
    });

    Route::prefix('cities')->name('cities.')->group(function () {
        Route::get('/', [ApiCityController::class, 'getCities']);
    });

    Route::prefix('design_image')->name('design_image.')->group(function () {
        Route::get('/', [ApiDesignImageController::class, 'getImage']);
    });

    Route::prefix('image')->name('image.')->group(function () {
        Route::get('/', [ApiImageController::class, 'index']);
    });
});




